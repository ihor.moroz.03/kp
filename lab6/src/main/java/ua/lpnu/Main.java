package ua.lpnu;

import lombok.SneakyThrows;
import ua.lpnu.model.Administrator;
import ua.lpnu.model.Book;
import ua.lpnu.model.Library;
import ua.lpnu.model.Subscription;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {

    @SneakyThrows
    private static Library load() {
        FileInputStream fileInputStream
                = new FileInputStream("data.txt");
        ObjectInputStream objectInputStream
                = new ObjectInputStream(fileInputStream);
        Library library = (Library) objectInputStream.readObject();
        objectInputStream.close();
        return library;
    }

    public static int createRandomIntBetween(int start, int end) {
        return start + (int) Math.round(Math.random() * (end - start));
    }

    public static Date createRandomDate(int startYear, int endYear) {
        int day = createRandomIntBetween(1, 28);
        int month = createRandomIntBetween(1, 12);
        int year = createRandomIntBetween(startYear, endYear);
        LocalDate dateToConvert = LocalDate.of(year, month, day);
        return convertLocalDateToDate(dateToConvert);
    }

    private static Date convertLocalDateToDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }

    private static Administrator.Note bookToNote(Book book) {
        final Date date = createRandomDate(2021, 2022);
        final Date returningDate = convertLocalDateToDate(LocalDate.ofInstant(date.toInstant(), ZoneId.systemDefault())
                .plusDays(30));
        return new Administrator.Note(book, date, returningDate);
    }

    @SneakyThrows
    private static void generate(Library library) {
        List<Book> bookList = List.of(
                new Book("author1", "name1", 2000),
                new Book("author1", "name2", 2001),
                new Book("author3", "name3", 2002),
                new Book("author4", "name4", 2003),
                new Book("author5", "name5", 2004),
                new Book("author6", "name6", 2005)
        );
        List<Subscription> subscriptionList = List.of(
                new Subscription("firstName1", "lastName1", "middleName1", "email1@gmail.com"),
                new Subscription("firstName2", "lastName2", "middleName2", "email2@gmail.com"),
                new Subscription("firstName3", "lastName3", "middleName3", "email3@gmail.com")
        );
        Subscription subscription1 = subscriptionList.get(0);
        List<Book> subscription1TakenBooks = bookList.subList(0, 3);
        subscription1.getTakenBooks()
                .addAll(subscription1TakenBooks);

        Subscription subscription2 = subscriptionList.get(1);
        List<Book> subscription2TakenBooks = bookList.subList(3, 4);
        subscription2.getTakenBooks()
                .addAll(subscription2TakenBooks);

        Subscription subscription3 = subscriptionList.get(2);
        List<Book> subscription3TakenBooks = bookList.subList(4, 6);
        subscription3.getTakenBooks()
                .addAll(subscription3TakenBooks);

        library.getBooks()
                .addAll(bookList);
        library.getSubscriptions()
                .addAll(subscriptionList);
        library.getAdministrator()
                .getNotes()
                .put(subscription1, subscription1TakenBooks.stream()
                        .map(Main::bookToNote)
                        .collect(Collectors.toList()));
        library.getAdministrator()
                .getNotes()
                .put(subscription2, subscription2TakenBooks.stream()
                        .map(Main::bookToNote)
                        .collect(Collectors.toList()));
        library.getAdministrator()
                .getNotes()
                .put(subscription3, subscription3TakenBooks.stream()
                        .map(Main::bookToNote)
                        .collect(Collectors.toList()));


        ObjectOutputStream objectOutputStream
                = new ObjectOutputStream(new FileOutputStream("data.txt"));
        objectOutputStream.writeObject(library);
        objectOutputStream.flush();
        objectOutputStream.close();
    }

    public static void main(String[] args) {
        int option;
        Scanner scanner = new Scanner(System.in);
        Library library = load();
//        Library library = new Library();
//        generate(library);
        System.out.println("1. Відсортувати всі книги за роком видання.");
        System.out.println("2. Створити список адресів для розсилки повідомлень для читачів, що взяли більше ніж 2 книги.");
        System.out.println("3. Перевірити, скільки користувачів взяли книги заданого автора.");
        System.out.println("4. Знайти найбільшу кількість книг, взятого читачем бібліотеки.");
        System.out.println("5. Здійснити різну розсилку 2 групам користувачів.");
        System.out.println("6. Створити список боржників на біжучу дату.");
        while ((option = scanner.nextInt()) != 0) {
            if (option == 1) {
                List<String> list = library.getBooksSortByYear()
                        .stream()
                        .map(Book::toString)
                        .toList();
                System.out.println(String.join("," + System.lineSeparator(), list));
            } else if (option == 2) {
                List<String> emails = library.getUsersEmails();
                System.out.println(String.join(",", emails));
            } else if (option == 3) {
                String author = scanner.next();
                System.out.println(library.getCountTakenBooksWithSpecificAuthor(author));
            } else if (option == 4) {
                System.out.println(library.getMaxTakenBooksCount());
            } else if (option == 5) {
                library.notifyGroups();
            } else if (option == 6) {
                Map<Subscription, List<String>> booksDebtors = library.getBooksDebtors();
                booksDebtors
                        .forEach((key, value) -> System.out.println(key + " - " + System.lineSeparator() + String.join("," + System.lineSeparator(), value)));
//                System.out.println(library.getBooksDebtors());
            }
        }
    }
}