package ua.lpnu.model;

import lombok.Data;

import java.io.Serializable;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Data
public class Library implements Serializable {
    private List<Book> books = new ArrayList<>();
    private List<Subscription> subscriptions = new ArrayList<>();
    private Administrator administrator = new Administrator();

    public List<Book> getBooksSortByYear() {
        return books.stream()
                .sorted(Comparator.comparing(Book::getYear))
                .collect(Collectors.toList());
    }

    public List<String> getUsersEmails() {
        Predicate<Subscription> subscriptionPredicate = subscription -> subscription.getTakenBooks()
                .size() > 2;
        return subscriptions.stream()
                .filter(subscriptionPredicate)
                .map(Subscription::getEmail)
                .collect(Collectors.toList());
    }

    public long getCountTakenBooksWithSpecificAuthor(String author) {
        Predicate<Book> bookPredicate = book -> book.getAuthor()
                .equals(author);
        return subscriptions.stream()
                .map(Subscription::getTakenBooks)
                .flatMap(Collection::stream)
                .filter(bookPredicate)
                .count();
    }

    public long getMaxTakenBooksCount() {
        return subscriptions.stream()
                .map(Subscription::getTakenBooks)
                .mapToLong(List::size)
                .max()
                .orElseThrow(IllegalStateException::new);
    }

    public void notifyGroups() {
        List<Subscription> firstGroup = subscriptions.stream()
                .filter(subscription -> subscription.getTakenBooks()
                        .size() < 2)
                .toList();
        List<Subscription> secondBooks = subscriptions.stream()
                .filter(subscription -> subscription.getTakenBooks()
                        .size() >= 2)
                .toList();
        List<Book> newBooks = books.stream()
                .limit(3)
                .toList();
        firstGroup.forEach(subscription -> subscription.reportAboutNewBooks(newBooks));
        secondBooks.forEach(Subscription::reportAboutReturningBooks);
    }

    public Map<Subscription, List<String>> getBooksDebtors() {
        return subscriptions.stream()
                .collect(Collectors.toMap(Function.identity(), this::getBookWithDays));
    }

    private List<String> getBookWithDays(Subscription key) {
        return administrator.getNotes()
                .get(key)
                .stream()
                .filter(note -> note.getReturnDate()
                        .before(Date.from(Instant.now())))
                .map((v) -> v.getBook() + ":" + Duration.between(v.getReturnDate()
                                .toInstant(), Instant.now())
                        .toDays() + " days")
                .toList();
    }

}
