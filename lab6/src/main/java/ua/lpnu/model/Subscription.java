package ua.lpnu.model;

import lombok.Data;
import lombok.NonNull;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@ToString(exclude = "takenBooks")
public class Subscription implements Serializable {
    @NonNull
    private String firstName;
    @NonNull
    private String lastName;
    @NonNull
    private String middleName;
    @NonNull
    private String email;
    @NonNull
    private List<Book> takenBooks = new ArrayList<>();

    public void reportAboutNewBooks(List<Book> books) {
        System.out.println("I have been notified with new books");
        System.out.println(books);
    }

    public void reportAboutReturningBooks() {
        System.out.println("Don't forget to return books!");

    }
}
