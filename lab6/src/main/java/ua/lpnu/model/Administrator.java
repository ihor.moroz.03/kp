package ua.lpnu.model;

import lombok.Data;
import lombok.NonNull;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class Administrator implements Serializable {

    @Data
    public static class Note implements Serializable {
        @NonNull
        private Book book;
        @NonNull
        private Date receivingDate;
        @NonNull
        private Date returnDate;
        @Setter
        private Date realReturnDate;
    }

    private Map<Subscription, List<Note>> notes = new HashMap<>();
}
