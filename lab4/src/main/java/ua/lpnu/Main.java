package ua.lpnu;

import java.util.*;
import java.util.regex.Pattern;

public class Main {
    public static final String EMAIL_PATTERN = "\\b[\\w._-]+@[\\w.]+\\b";

    public static void main(String[] args) {
        String text = """
RegEr was created by ih.or@gskinner.com.

Edit the Expression & Text to see matches. R Oll over matches or the expression for details. PCRE & JavaScript flavors of RegEx are supported. Validate your expression with Tests mode.

The side bar includes a Cheats@heet , full Refer ihor.moroz.pz.2022@lpnu.ua ence, and Help. You can also Save & Share with the Community and view patterns you areat e or favorite in My Patterns.

Explore results with the Tools below. Replace & List output custom results. Details lists capture groups. Explain describes your expression in plain Engli. LAST SENTENCE.
                """;

        String[] sentences = text.split("(\\. )|(\\.\n)");
        String lastSentence = sentences[sentences.length - 1];

        List<String> candicates = new ArrayList<>();

        for (int i = 0; i < sentences.length; i++) {
            String[] words = sentences[i].split(" ");
            List<String> candidatesPerSentence = Arrays.stream(words).filter(str -> str.contains("@")).toList();
            candicates.addAll(candidatesPerSentence);


            if (candidatesPerSentence.stream().anyMatch(s -> Pattern.compile(EMAIL_PATTERN).matcher(s).matches()))
                sentences[i] = lastSentence;
        }

        System.out.println("Candidates:");
        for (String s : candicates)
            System.out.println(s);

        System.out.println();
        System.out.println("Output:");
        String output = String.join(". ", sentences);
        System.out.print(output);
        System.out.println();
    }
}
