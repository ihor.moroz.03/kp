package ua.lpnu;

public record Employee(String surname, int birthYear, Salary salary, Position position) {

}
