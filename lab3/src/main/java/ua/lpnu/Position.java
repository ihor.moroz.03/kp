package ua.lpnu;

public enum Position {
    Producer,
    Cleaner,
    Assembler
}
