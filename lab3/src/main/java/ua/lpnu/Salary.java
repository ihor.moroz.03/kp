package ua.lpnu;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Salary {
    MIN(100D), AVG(200D), MAX(300D);

    @Getter
    private final Double salary;
}
