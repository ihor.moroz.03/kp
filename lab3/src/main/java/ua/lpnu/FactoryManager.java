package ua.lpnu;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;

import java.io.File;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

public class FactoryManager {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static final ArrayList<Employee> employees = new ArrayList<>();

    public static void main(String[] args) {
        getSources().forEach(FactoryManager::generate);
        getSources().forEach(FactoryManager::load);
        HashMap<Position, List<Employee>> map = getMapByPositions();
        Employee oldestProducer = FindExtremeByAge(map.get(Position.Producer), Comparator.comparingInt(Employee::birthYear));
        Employee oldestAssembler = FindExtremeByAge(map.get(Position.Assembler), Comparator.comparingInt(Employee::birthYear));
        Employee oldestCleaner = FindExtremeByAge(map.get(Position.Cleaner), Comparator.comparingInt(Employee::birthYear));
        Employee youngestProducer = FindExtremeByAge(map.get(Position.Producer), (a, b) -> b.birthYear() - a.birthYear());
        Employee youngestAssembler = FindExtremeByAge(map.get(Position.Assembler), (a, b) -> b.birthYear() - a.birthYear());
        Employee youngestCleaner = FindExtremeByAge(map.get(Position.Cleaner), (a, b) -> b.birthYear() - a.birthYear());

        System.out.println("Oldest Producer");
        System.out.println(oldestProducer);

        System.out.println("Oldest Assembler");
        System.out.println(oldestAssembler);

        System.out.println("Oldest Cleaner");
        System.out.println(oldestCleaner);

        System.out.println("Youngest Producer");
        System.out.println(youngestProducer);

        System.out.println("Youngest Assembler");
        System.out.println(youngestAssembler);

        System.out.println("Youngest Cleaner");
        System.out.println(youngestCleaner);

    }

    public static List<String> getSources() {
        return List.of("lab3/employees1.json", "lab3/file.json");
    }

    @SneakyThrows
    public static void load(String path) {
        TypeReference<List<Employee>> typeRef = new TypeReference<>() {
        };
        List<Employee> list = objectMapper.readValue(new File(path), typeRef);
        employees.addAll(list);
    }

    @SneakyThrows
    public static void generate(String path) {
        File file = new File(path);
        Files.deleteIfExists(file.toPath());
        Random random = new Random();
        final int lengthSalary = Salary.values().length;
        final int lengthPosition = Position.values().length;
        List<Employee> list = random.ints(10, 1, 10)
                .mapToObj(mapper -> new Employee("Surname" + mapper, 1998 + mapper,
                        Salary.values()[random.nextInt(lengthSalary)], Position.values()[random.nextInt(lengthPosition)]))
                .collect(Collectors.toList());
        objectMapper.writeValue(file, list);
    }

    public static HashMap<Position, List<Employee>> getMapByPositions() {
        HashMap<Position, List<Employee>> res = new HashMap<>();

        res.put(Position.Producer, new ArrayList<>());
        res.put(Position.Assembler, new ArrayList<>());
        res.put(Position.Cleaner, new ArrayList<>());

        for (Employee employee : employees) {
            res.get(employee.position())
                    .add(employee);
        }

        return res;
    }

    public static Employee FindExtremeByAge(List<Employee> arr, Comparator<Employee> comparator) {
        arr.sort(comparator);
        return arr.get(0);
    }
}