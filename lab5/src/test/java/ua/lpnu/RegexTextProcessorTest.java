package ua.lpnu;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class RegexTextProcessorTest {

    @Test
    public void filterWordsTestEmpty() {
        RegexTextProcessor regexTextProcessor = new RegexTextProcessor();
        assertThrows(IllegalArgumentException.class, () -> regexTextProcessor.filterWords("", 0));
    }

    @Test
    public void filterWordsTest() {
        String input = """
                Edit the Expression & Text to see matches. Roll over matches or the expression for details. PCRE & JavaScript flavors of RegEx are supported. Validate your expression with Tests mode.

                The side bar includes a Cheatsheet, full Reference, and Help. You can also Save & Share with the Community and view patterns you areat e or favorite in My Patterns.

                Explore results with the Tools below. Replace & List output custom results. Details lists capture groups. Explain describes your expression in plain English .
                """;
        String expected = """
                Edit the Expression &  to see matches.  over matches or the expression for details.  & JavaScript flavors of RegEx are supported. Validate your expression  Tests .

                The  bar includes a Cheatsheet,  Reference, and . You can also  & Share  the Community and  patterns you areat e or favorite in My Patterns.

                Explore results  the Tools below. Replace &  output custom results. Details lists capture groups. Explain describes your expression in plain English .
                """;
        RegexTextProcessor regexTextProcessor = new RegexTextProcessor();
        String word = regexTextProcessor.filterWords(input, 4);
        assertEquals(expected, word);
    }

    @Test
    public void getUpperCaseWordsTriplesTest() {
        String input = """
                Edit the Expression & Text to see matches. R Oll over matches or the expression for details. PCRE & JavaScript flavors of RegEx are supported. Validate your expression with Tests mode.

                The side bar includes a Cheatsheet, full Reference, and Help. You can also Save & Share with the Community and view patterns you areat e or favorite in My Patterns.

                Explore results with the Tools below. Replace & List output custom results. Details lists capture groups. Explain describes your expression in plain English .
                """;

        RegexTextProcessor regexTextProcessor = new RegexTextProcessor();
        List<List<String>> triples = regexTextProcessor.getUpperCaseWordsTriples(input);
        List<List<String>> expected = List.of(
                List.of("Edit", "Expression", "Text"),
                List.of("R", "Oll", "PCRE"),
                List.of("JavaScript", "RegEx", "Validate"),
                List.of("Tests", "The", "Cheatsheet"),
                List.of("Reference", "Help", "You"),
                List.of("Save", "Share", "Community"),
                List.of("My", "Patterns", "Explore"),
                List.of("Tools", "Replace", "List"),
                List.of("Details", "Explain", "English")
        );
        assertEquals(expected, triples);
    }

}
