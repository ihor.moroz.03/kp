package ua.lpnu;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexTextProcessor {

    public String filterWords(String input, int wordLength) {
        final String pattern = "\\b[^aeiouyAEIOUY\\W](\\w){" + (wordLength - 1) + "}\\b";

        if (input.trim()
                .isEmpty()) {
            throw new IllegalArgumentException("Input data is empty");
        }
        if (!Pattern.compile(pattern)
                .matcher(input)
                .find()) {
            throw new IllegalArgumentException("No lexemes were found");
        }

        int lastIndex = 0;
        StringBuilder output = new StringBuilder();
        Matcher matcher = Pattern.compile(pattern)
                .matcher(input);

        while (matcher.find()) {
            output.append(input, lastIndex, matcher.start());
            lastIndex = matcher.end();
        }
        if (lastIndex < input.length()) {
            output.append(input, lastIndex, input.length());
        }

        return output.toString();
    }

    public List<List<String>> getUpperCaseWordsTriples(String input) {
        final String pattern = "\\b[A-Z]\\w*\\b";

        if (input.trim()
                .isEmpty()) {
            throw new IllegalArgumentException("Input data is empty");
        }
        if (!Pattern.compile(pattern)
                .matcher(input)
                .find()) {
            throw new IllegalArgumentException("No lexemes were found");
        }

        List<List<String>> triples = new ArrayList<>();

        var results = Pattern.compile(pattern)
                .matcher(input)
                .results()
                .toList();

        int i = 0;
        for (MatchResult result : results) {
            if (i == 0) triples.add(new ArrayList<>());

            triples.get(triples.size() - 1)
                    .add(result.group());
            i++;
            i %= 3;
        }

        return triples;
    }
}
