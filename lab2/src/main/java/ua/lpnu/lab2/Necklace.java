package ua.lpnu.lab2;

import lombok.Getter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
public class Necklace {
    private final List<Stone> stones;
    private final BigDecimal price;

    public Necklace(List<Stone> stones, BigDecimal price) {
        this.stones = new ArrayList<>(stones);
        this.price = price;
    }
}
