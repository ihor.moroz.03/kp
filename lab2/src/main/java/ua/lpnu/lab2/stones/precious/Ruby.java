package ua.lpnu.lab2.stones.precious;

import ua.lpnu.lab2.stones.PreciousStone;

public class Ruby extends PreciousStone {
    public Ruby(int weight) {
        super(weight, 5, "Ruby");
    }
}
