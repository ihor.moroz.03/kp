package ua.lpnu.lab2.stones.semiprecious;

import ua.lpnu.lab2.stones.SemiPreciousStone;

public class Tourmaline extends SemiPreciousStone {
    public Tourmaline(int weight) {
        super(weight, 3, "Tourmaline");
    }
}
