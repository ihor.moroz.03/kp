package ua.lpnu.lab2.stones.precious;

import ua.lpnu.lab2.stones.PreciousStone;

public class Sapphire extends PreciousStone {
    public Sapphire(int weight) {
        super(weight, 3, "Sapphire");
    }
}
