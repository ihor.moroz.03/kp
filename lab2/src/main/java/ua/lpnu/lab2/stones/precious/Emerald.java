package ua.lpnu.lab2.stones.precious;

import ua.lpnu.lab2.stones.PreciousStone;

public class Emerald extends PreciousStone {
    public Emerald(int weight) {
        super(weight, 5, "Emerald");
    }
}
