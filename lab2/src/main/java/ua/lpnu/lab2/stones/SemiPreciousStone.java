package ua.lpnu.lab2.stones;

import ua.lpnu.lab2.Stone;

public class SemiPreciousStone extends Stone {
    public SemiPreciousStone(int weight, int transparency, String name) {
        super(weight, transparency, name);
    }
}
