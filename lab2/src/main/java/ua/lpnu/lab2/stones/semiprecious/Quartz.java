package ua.lpnu.lab2.stones.semiprecious;

import ua.lpnu.lab2.stones.SemiPreciousStone;

public class Quartz extends SemiPreciousStone {
    public Quartz(int weight) {
        super(weight, 4, "Quartz");
    }
}
