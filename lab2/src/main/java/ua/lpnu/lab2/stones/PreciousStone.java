package ua.lpnu.lab2.stones;

import ua.lpnu.lab2.Stone;

public class PreciousStone extends Stone {
    public PreciousStone(int weight, int transparency, String name) {
        super(weight, transparency, name);
    }
}
