package ua.lpnu.lab2;

import lombok.Getter;
import lombok.SneakyThrows;
import ua.lpnu.lab2.stones.precious.Emerald;
import ua.lpnu.lab2.stones.precious.Ruby;
import ua.lpnu.lab2.stones.precious.Sapphire;
import ua.lpnu.lab2.stones.semiprecious.Quartz;
import ua.lpnu.lab2.stones.semiprecious.Tourmaline;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class NecklaceManager {

    /*
    1. Використовувати можливості ООП: класи, успадкування, поліморфізм, інкапсуляцію.
    2. Кожний клас повинен мати назву, яка повністю описує його суть, і інформативний склад.
    Атрибути і методи класів слід визначити самостійно.
    3. Успадкування потрібно використовувати тільки тоді, коли воно має сенс. У випадку
    використання наслідування кількість класів-нащадків має бути не меншою 2 і не
    більшою 4-ьох
    4. При записі програми потрібно використовувати домовленості щодо оформлення коду
    java code convention.
    5. Зображати всі пари set/get (ака сеттери/геттери) для атрибутів класу не потрібно з
    метою уникнення засмічення діаграми
    6. Для реалізації операцій пошуку/сортування слід реалізувати окремий клас (в назві якого
    має бути присутнє слово Manager)
    7. Реалізувати один з методів сортування з використанням компаратора, реалізованого
    як статичний вкладений клас (static inner class)
    8. Реалізувати наступний з методів сортування з використанням компаратора,
    реалізованого як вкладений клас (inner class)
    9. Додати ще один метод сортування (додатковий), який реалізує сортування з
    використанням анонімного класу (anonymous inner class)
    10. Додати ще один, 4-й метод сортування з використанням лямбда-виразів
    * */

    public enum Direction {
        ASC, DESC
    }

    @Getter
    private final List<Necklace> necklaces = new ArrayList<>();

    private static final List<Class<? extends Stone>> STONES = List.of(Emerald.class, Ruby.class, Sapphire.class, Quartz.class, Tourmaline.class);

    @SneakyThrows
    public static List<Stone> generateRandomStones() {
        Random random = new Random();
        List<Stone> stones = new ArrayList<>();
        int count = random.nextInt(5) + 5;
        for (int i = 0; i < count; i++) {
            Stone stone = STONES.get(random.nextInt(STONES.size()))
                    .getDeclaredConstructor(int.class)
                    .newInstance(random.nextInt(50) + 1);
            stones.add(stone);
        }
        return stones;
    }

    public static void main(String[] args) {
        NecklaceManager necklaceManager = new NecklaceManager();
        Random random = new Random();
        int count = random.nextInt(10) + 5;
        for (int i = 0; i < count; i++) {
            necklaceManager.getNecklaces()
                    .add(new Necklace(generateRandomStones(), BigDecimal.valueOf(random.nextDouble() * 100)));
        }
    }

    public List<Stone> getStonesInNecklaceByTransparencyRange(int start, int end) {
        Predicate<Necklace> necklacePredicate = p -> p.getStones()
                .stream()
                .map(Stone::getTransparency)
                .allMatch(transparency -> transparency > start && transparency < end);
        return necklaces.stream()
                .filter(necklacePredicate)
                .map(Necklace::getStones)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    public List<Necklace> sortByWeight(Direction direction) {
        ArrayList<Necklace> copy = new ArrayList<>(necklaces);
        if (direction == Direction.ASC) {
            copy.sort(new NecklaceWeightComparator());
        } else {
            copy.sort(new Comparator<Necklace>() {
                @Override
                public int compare(Necklace o1, Necklace o2) {
                    int weight1 = o1.getStones()
                            .stream()
                            .mapToInt(Stone::getWeight)
                            .sum();
                    int weight2 = o2.getStones()
                            .stream()
                            .mapToInt(Stone::getWeight)
                            .sum();
                    return Integer.compare(weight1, weight2);
                }
            }.reversed());
        }
        return copy;
    }

    public List<Necklace> sortByPrice(Direction direction) {
        ArrayList<Necklace> copy = new ArrayList<>(necklaces);
        if (direction == Direction.ASC) {
            copy.sort(new NecklacePriceComparator());
        } else if (direction == Direction.DESC) {
            copy.sort(((o1, o2) -> {
                return o1.getPrice()
                        .compareTo(o2.getPrice());
            }));
        }
        return copy;
    }


    private static class NecklaceWeightComparator implements Comparator<Necklace> {
        @Override
        public int compare(Necklace o1, Necklace o2) {
            int weight1 = o1.getStones()
                    .stream()
                    .mapToInt(Stone::getWeight)
                    .sum();
            int weight2 = o2.getStones()
                    .stream()
                    .mapToInt(Stone::getWeight)
                    .sum();
            return Integer.compare(weight1, weight2);
        }
    }


    private class NecklacePriceComparator implements Comparator<Necklace> {
        @Override
        public int compare(Necklace o1, Necklace o2) {
            return o1.getPrice()
                    .compareTo(o2.getPrice());
        }
    }
}
