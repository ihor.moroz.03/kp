package ua.lpnu.lab2;

import lombok.Getter;

@Getter
public abstract class Stone {
    private final int weight;
    private final int transparency;
    private final String name;

    protected Stone(int weight, int transparency, String name) {
        this.weight = weight;
        this.transparency = transparency;
        this.name = name;
    }
}
